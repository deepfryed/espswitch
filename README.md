# ESP8266 Switch

A simple ESP8266 based switch with latched on / off to save battery.

## 3D

![](3d/espswitch-3d-top.png)
![](3d/espswitch-3d-bot.png)
![](enclosure/espswitch-enclosure.png)

## LICENCE

[TAPR Open Hardware License](https://en.wikipedia.org/wiki/TAPR_Open_Hardware_License)
